﻿using MediatR;
using Milestone.BusinessLayer.Services;
using Milestone.Models;
using Milestone.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Milestone.Handlers
{
    public class GetMenuBarHandler : IRequestHandler<GetMenuBarQuery, IEnumerable<MenuBar>>
    {
        IGroceries _data;

        public GetMenuBarHandler(IGroceries data)
        {
            _data = data;
        }

        public async Task<IEnumerable<MenuBar>> Handle(GetMenuBarQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.GetAllCategories());
        }
    }
}
