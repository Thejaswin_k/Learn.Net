﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thejaswin_MVCWebAPI_17Aug.Models
{
    public class Automobile
    {
        public int cardID { get; set; }
        public String carname { get; set; }
        public int carProduction { get; set; }
        public string carType { get; set; }
    }
}
