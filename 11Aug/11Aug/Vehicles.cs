﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _11Aug
{
    abstract class Vehicles
    {
        public string ownerName;
        public string vehicleName;

        abstract public void OwnerName();
    }

    class Cars : Vehicles
    {
        public void nameofVehicle()
        {
            Console.WriteLine("Name of the Owner is " + vehicleName);
        }

        public override void OwnerName()
        {
            Console.WriteLine("Name of the Owner is " + ownerName);
        }

    }
}
