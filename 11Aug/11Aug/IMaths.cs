﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _11Aug
{
    interface IMaths
    {
        public int Addition(int firstValue, int secondvalue);
        public int Subtraction(int firstValue, int secondvalue);
        public int Multiplication(int firstValue, int secondvalue);
        public int Division(int firstValue, int secondvalue);
    }
}
