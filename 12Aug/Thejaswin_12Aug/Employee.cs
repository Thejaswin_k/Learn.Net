﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thejaswin_12Aug
{
    class Employee
    {
        private int _id;
        private string  _name;
        private double _phno;
        
        public Employee(int id,string name,double phno)
        {
            _id = id;
            _name = name;
            _phno = phno;

        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double Phno
        {
            get { return _phno; }
            set { _phno = value; }
        }
    }
}
