﻿using System;

namespace _11Aug
{ 
    class Program
    {

        static void Main(string[] args)
        {

            try
            {
                Ans solution = new Ans();
                Console.WriteLine("Enter First Value: ");
                int value1 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Second Value: ");
                int value2 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine(solution.Addition(value1, value2));
                Console.WriteLine(solution.Subtraction(value1, value2));
                Console.WriteLine(solution.Division(value1, value2));
                Console.WriteLine(solution.Multiplication(value1, value2));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine("Something went wrong!");
            }
        }
    }
}
