﻿using System.Collections.Generic;
using System.Net.Http;
using Thejaswin_Swagger_Examples.Models;

namespace Thejaswin_Swagger_Examples.IServices
{
     public interface IStudentServices
    {
        List<Student> GetAllStudents();
        Student GetStudentbyId(int id);
        List<Student> DeleteStudentbyId(int id);
        List<Student> AddStudent(Student student);
 




    }
}
