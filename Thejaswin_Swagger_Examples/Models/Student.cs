﻿namespace Thejaswin_Swagger_Examples.Models
{
    public class Student
    {
        public int studentId { get; set; }
        public string studentName { get; set; }
        public int studentRollNo { get; set; }
    }
}
