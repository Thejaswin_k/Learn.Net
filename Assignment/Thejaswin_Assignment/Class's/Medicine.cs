﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thejaswin_Assignment
{
    class Medicine
    {
        private int id;
        private string medName;
        private DateTime DateofExp;
        private string seller;

        public Medicine(int id, string medName, DateTime dateofExp, string seller)
        {
            this.id = id;
            this.medName = medName;
            DateofExp = dateofExp;
            this.seller = seller;
        }

        public int Id { get => id; set => id = value; }
        public string MedName { get => medName; set => medName = value; }
        public DateTime DateofExp1 { get => DateofExp; set => DateofExp = value; }
        public string Seller { get => seller; set => seller = value; }
    }
}
