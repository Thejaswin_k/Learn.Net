import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookAppointmentComponent } from './book-appointment/book-appointment.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { QueryTableComponent } from './query-table/query-table.component';
import { ViewAppointmentComponent } from './view-appointment/view-appointment.component';

const routes: Routes = [
  {path:"",component:LandingPageComponent},
  {path:"book-appointment",component:BookAppointmentComponent},
  {path:"view-appointment",component:ViewAppointmentComponent},
  {path:"contact-us",component:ContactUsComponent},
  {path:"query-table",component:QueryTableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
