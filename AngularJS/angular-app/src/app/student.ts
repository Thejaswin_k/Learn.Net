export default class student {
  student_id: string;
  student_name: string;
  student_class: string;

  constructor (student_id: string, student_name: string, student_class: string) {
    this.student_id = student_id;
    this.student_name = student_name;
    this.student_class = student_class;
  }
}
