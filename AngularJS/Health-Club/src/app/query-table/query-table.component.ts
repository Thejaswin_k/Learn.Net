import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.services';

@Component({
  selector: 'app-query-table',
  templateUrl: './query-table.component.html',
  styleUrls: ['./query-table.component.scss']
})
export class QueryTableComponent implements OnInit {
  url="http://localhost:3000/query";

  constructor(public api:ApiService) { }
  queries:any;

  ngOnInit(): void {
    this.api.get(this.url).subscribe(res=>{
      this.queries=res;
      console.log(res);
    }) 
  }
  trackByIndex = (index:number):number =>{
    return index;
  }

}
