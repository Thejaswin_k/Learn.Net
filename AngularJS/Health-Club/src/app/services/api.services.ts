import { Injectable } from '@angular/core';
import { HttpClient,HttpParams,HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  JSON: any;

  constructor(private http: HttpClient) { }  
  
  public getString (object: any) {
    let paramsAndValues = [];
    for(let key in object) {
      let value = encodeURIComponent(object[key].toString());
      paramsAndValues.push([key, value].join('='));
    }
    return paramsAndValues.join('&');
  }
  public get(url: string, data?: any, options?: any) {
    console.log(data);

    return this.http.get(url +"?"+ this.getString(data), options);
  }  
  
  public post(url: string, data?: any, options?: any) {
    // data = this.postString(data);
    return this.http.post(url, data, options);
  }  

}