﻿using MediatR;
using Milestone.Models;

namespace Milestone.Queries
{
    public class GetOrderInfoQuery : IRequest<ProductOrder>
    {
        public int OrderId { get; set; }
    }
}
