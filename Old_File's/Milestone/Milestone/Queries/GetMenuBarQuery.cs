﻿using MediatR;
using Milestone.Models;
using System.Collections.Generic;

namespace Milestone.Queries
{
    public class GetMenuBarQuery : IRequest<IEnumerable<MenuBar>>
    {
        /*
            No input 
        */
    }
}
