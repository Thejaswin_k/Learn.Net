﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thejaswin_12Aug
{
    class GenericClass
    {
        public void GenericMethod<T1,T2>(T1 param1,T2 param2)
        {
            Console.WriteLine($"Parameter 1: {param1}: Parameter 2: {param2}");
 
        }

    }
}
