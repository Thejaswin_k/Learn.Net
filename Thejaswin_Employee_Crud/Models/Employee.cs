﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thejaswin_Employee_Crud.Models
{
    public class Employee
    {
        public Guid employeeGuid { get; set; }
        public string employeeName { get; set; }
        public string employeeAddress { get; set; }
        public string employeeDepartment { get; set; }
        public int employeePhno { get; set; }
    }
}
