﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thejaswin_Assignment
{
    class Gym
    {
        private int id;
        private string name;
        private DateTime startDate;
        private DateTime endDate;

        public Gym(int id, string name, DateTime startDate, DateTime endDate)
        {
            this.Id = id;
            this.name = name;
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public DateTime EndDate { get => endDate; set => endDate = value; }
        public DateTime StartDate { get => startDate; set => startDate = value; }
        public string Name { get => name; set => name = value; }
        public int Id { get => id; set => id = value; }
    }
}
