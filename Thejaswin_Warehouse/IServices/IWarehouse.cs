﻿using System.Collections.Generic;
using Thejaswin_Warehouse.Models;

namespace Thejaswin_Warehouse.IServices
{
    public interface IWarehouse
    {
        List<Warehouse> GetallItems();
        Warehouse PickitemById(int id);
        List<Warehouse> AddItem(Warehouse warehouse);
        List<Warehouse> DeletebyId(int id);
        Warehouse UpdateNew(int id);
    }
}
