﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Thejaswin_Swagger_Examples.IServices;
using Thejaswin_Swagger_Examples.Models;

namespace Thejaswin_Swagger_Examples.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : Controller
    {
        IStudentServices _studentservices;

        public StudentController(IStudentServices studentServices)
        {
            _studentservices = studentServices;
        }

        [HttpGet]
        public List<Student> Get()
        {
            return _studentservices.GetAllStudents();

        }

        [HttpGet("{id}",Name = "Get")]
        public Student Get(int id)
        {
            return _studentservices.GetStudentbyId(id);
               
        }

        [HttpPost]
        public List<Student> Post([FromBody] Student student)
            
        {
            return _studentservices.AddStudent(student);

        }

        [HttpDelete("{id}")]
        public List<Student> DeletebyId(int id)
        {
            return _studentservices.DeleteStudentbyId(id);
        }
    }
}
