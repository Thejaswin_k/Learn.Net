﻿using System;

namespace Thejaswin_Sub_9Aug
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter First Number!");
            int firstValue = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Second Number!");
            int secondValue = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Results= " + (firstValue - secondValue));
        }
    }
}
