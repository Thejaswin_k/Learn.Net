﻿using System.Collections.Generic;

namespace Milestone.Models
{
    public partial class Product
    {
        public Product()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal? Amount { get; set; }
        public int? Stock { get; set; }
        public int? CatId { get; set; }
        public string Photo { get; set; }

        public virtual Categories Cat { get; set; }
        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
