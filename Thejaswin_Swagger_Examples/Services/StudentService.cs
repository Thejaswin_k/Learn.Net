﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Thejaswin_Swagger_Examples.IServices;
using Thejaswin_Swagger_Examples.Models;

namespace Thejaswin_Swagger_Examples.Services
{
    public class StudentService : IStudentServices
    {
        List<Student> StudentList = new List<Student>();
        public StudentService()
        {
            for (int i = 1; i <= 9; i++)
            {
                StudentList.Add(new Student()
                {
                    studentId = i,
                    studentName = "Student" + i,
                    studentRollNo = 100+ i
                });
            }
        }
        public List<Student> AddStudent(Student student)
        {
            StudentList.Add(student);
            return StudentList;
        }

        public List<Student> DeleteStudentbyId(int id)
        {
            StudentList.RemoveAll(x => x.studentId == id);
            return StudentList;
        }

        public Student GetStudentbyId(int id)
        {
            return StudentList.SingleOrDefault(x => x.studentId == id);
        }

        public List<Student> GetAllStudents()
        {
            return StudentList;
        }
    }
}

