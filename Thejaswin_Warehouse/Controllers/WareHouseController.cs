﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Thejaswin_Warehouse.IServices;
using Thejaswin_Warehouse.Models;

namespace Thejaswin_Warehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WareHouseController : Controller
    {
        IWarehouse _warehouseservices;

        public WareHouseController(IWarehouse WServices)
        {
            _warehouseservices = WServices;
        }

        [HttpGet]
        public List<Warehouse> Get()
        {
            return _warehouseservices.GetallItems();

        }

        [HttpGet("{id}", Name = "Get")]
        public Warehouse GetbyId(int id)
        {
            return _warehouseservices.PickitemById(id);
        }

        [HttpDelete("{id}", Name = "Delete")]
        public List<Warehouse> Delete(int id)
        {
            return _warehouseservices.DeletebyId(id);
        }

        [HttpPost]
        public List<Warehouse> Post([FromBody] Warehouse warehouse)

        {
            return _warehouseservices.AddItem(warehouse);

        }

        [HttpPut]
        public Warehouse UpdateItem([FromBody] int id)
        {
            return _warehouseservices.UpdateNew(id);
        }

    }
}
