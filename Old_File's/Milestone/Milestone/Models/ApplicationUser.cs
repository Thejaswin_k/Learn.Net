﻿using System.Collections.Generic;

namespace Milestone.Models
{
    public partial class ApplicationUser
    {
        public ApplicationUser()
        {
            ProductOrder = new HashSet<ProductOrder>();
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public long? MobileNumber { get; set; }
        public int? PinCode { get; set; }
        public string HouseNoBuldingName { get; set; }
        public string RoadArea { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public virtual ICollection<ProductOrder> ProductOrder { get; set; }
    }
}
