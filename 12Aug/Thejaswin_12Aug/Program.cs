﻿using System;
using System.Collections;
using System.IO;



namespace Thejaswin_12Aug
{
    class Program
    {
        class FileWrite
        {
            public void write()
            {
                FileStream fs = new FileStream(@"E:\\New_File", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                Console.WriteLine("Enter Text");
                string str = Console.ReadLine();
                sw.WriteLine(str);
                sw.Flush();
                sw.Close();

            }
        }
        Hashtable hashList = new Hashtable();

        public void addHash()
        {
           
            hashList.Add(1, "Thejaswin");
            hashList.Add(2, "Kannan");
            foreach (DictionaryEntry ans in hashList)
            {
                Console.WriteLine("Key: "+ans.Key.ToString()+" " +"Value: "+ ans.Value.ToString());

            }
           
        }
        public void containsKey(int key)

        {
            Console.WriteLine(hashList.ContainsKey(key));
        }

        public void CopyEx()
        {
            int[] sourceArr = new int[5] { 1,2,3,4,5};
            int[] destArr = new int[sourceArr.Length];

            sourceArr.CopyTo(destArr, 0);
           foreach(int num in destArr){
                Console.WriteLine(num);

            }
        }

        public void cloneEx()
        {
            string firstStr = "Thejaswin";
            string newStr = (String)firstStr.Clone();

            Console.WriteLine(newStr);

        }

        public void ArrEx()
        {
            ArrayList myArr = new ArrayList();
            myArr.Add("Thejaswin");
            myArr.Add("Mansi");
            myArr.Add("Sai");

            IEnumerator iEnum = myArr.GetEnumerator();
            while (iEnum.MoveNext())
            {
                Console.WriteLine(iEnum.Current.ToString());
            }

        }
        static void Main(string[] args)
        {
            //Collections
            /*
            Program Func = new Program();
            Func.ArrEx();
            Func.CopyEx();
            Func.cloneEx();
            Func.addHash();
            Func.containsKey(4);*/

            //Class
            /*
            List<Employee> Emp = new List<Employee>();
            Emp.Add(new Employee(1, "Thejaswin", 1001));
            Emp.Add(new Employee(2, "Kannan", 1002));
            Emp.Add(new Employee(3, "Shri", 1003));
            foreach(var emp in Emp)
            {
                if (emp.Name == "Thejaswin")
                {
                    emp.Name = "YOYO";
                }            
                Console.WriteLine("Employee Name: "+emp.Name+" "
                    +"Employee Id: "+emp.Id+" "+
                    "Employee Id: "+emp.Phno);
            }
            */
            //File Handling
            /*
            FileWrite wr = new FileWrite();
            wr.write();
            */

            //Generics
            GenericClass gc = new GenericClass();
            gc.GenericMethod(10, 78);
            gc.GenericMethod(90, "Thejaswin");

        }
    }
}
