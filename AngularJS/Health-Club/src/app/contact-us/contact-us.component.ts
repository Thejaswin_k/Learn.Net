import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../services/api.services';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  constructor(private formbuilder:FormBuilder,private api:ApiService) { }
url = "http://localhost:3000/query";
contactForm:FormGroup = this.formbuilder.group({
  name:["", [Validators.required]],
  email:["", [Validators.required]],
  phno:["", [Validators.required]],
  query:["", [Validators.required]]
})

  ngOnInit(): void {
  }

  postQuery(){
    alert("Thank You!")
    console.log("Working!")
    let time = Date.now();
    let body = {
      id: time,
      name:this.contactForm.value.name,
      email:this.contactForm.value.email,
      phno:this.contactForm.value.phno,
      query:this.contactForm.value.query
    }
    this.api.post(this.url,body).subscribe(res=>{
      console.log(res)
    })
    this.contactForm.reset();
  }

}
