﻿using System;
using System.Collections.Generic;
using System.Linq;
using Thejaswin_Warehouse.IServices;
using Thejaswin_Warehouse.Models;

namespace Thejaswin_Warehouse.Services
{

    public class WarehouseServices : IWarehouse
    {
        List<Warehouse> Warehouseitems = new List<Warehouse>();
        public WarehouseServices()
        {
            for (int i = 1; i <= 9; i++)
            {
                Warehouseitems.Add(new Warehouse()
                {
                    itemId = i,
                    itemName = "Item" + i,
                    itemExp = DateTime.Now,
                    itemStock = 100 - i
                });
            }
        }

        public List<Warehouse> AddItem(Warehouse warehouse)
        {
            Warehouseitems.Add(warehouse);
            return Warehouseitems;
        }

        public List<Warehouse> DeletebyId(int id)
        {
            Warehouseitems.RemoveAll(x => x.itemId == id);
            return Warehouseitems;
        }

        public List<Warehouse> GetallItems()
        {
            return Warehouseitems;
        }

        public Warehouse PickitemById(int id)
        {
            return Warehouseitems.SingleOrDefault(x => x.itemId == id);

        }

        public Warehouse UpdateNew(int id)
        {
            var exObj = Warehouseitems.SingleOrDefault(x => x.itemId == id);
            exObj.itemName = "Changed Item";
            return exObj;
        }
    }
}
