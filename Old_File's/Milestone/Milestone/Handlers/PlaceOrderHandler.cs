﻿using MediatR;
using Milestone.BusinessLayer.Services;
using Milestone.Commands;
using Milestone.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Milestone.Handlers
{
    public class PlaceOrderHandler : IRequestHandler<PlaceOrderCommand, ProductOrder>
    {
        IGroceries _data;

        public PlaceOrderHandler(IGroceries data)
        {
            _data = data;
        }
        async Task<ProductOrder> IRequestHandler<PlaceOrderCommand, ProductOrder>.Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.PlaceOrder(request.UserId, request.ProductId));
        }
    }

}
