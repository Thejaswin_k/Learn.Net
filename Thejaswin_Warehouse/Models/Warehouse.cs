﻿using System;

namespace Thejaswin_Warehouse.Models
{
    public class Warehouse
    {
        public int itemId { get; set; }
        public string itemName { get; set; }
        public DateTime itemExp { get; set; }
        public int itemStock { get; set; }
    }
}
