﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _11Aug
{
    abstract class Area
    {
        abstract public int squareArea();
    }

    class Square : Area
    {
        int side = 0;
        public Square(int n)
        {
            side = n;
        }

        public override int squareArea()
        {
            return side * side;

        }

    }
}