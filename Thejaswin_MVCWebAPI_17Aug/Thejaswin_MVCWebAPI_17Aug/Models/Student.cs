﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Thejaswin_MVCWebAPI_17Aug.Models
{
    public class Student
    {
        public int studentId { get; set; }
        public string studentName { get; set; }
        public double studentPhno { get; set; }

    }
}
