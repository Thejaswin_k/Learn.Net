import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.services';

@Component({
  selector: 'app-view-appointment',
  templateUrl: './view-appointment.component.html',
  styleUrls: ['./view-appointment.component.scss']
})
export class ViewAppointmentComponent implements OnInit {
  url = "http://localhost:3000/health-member";

  constructor(public api:ApiService) { }
  appointments:any;

  ngOnInit(): void {
    this.api.get(this.url).subscribe(res=>{
      this.appointments=res;
    })
  }
  trackByIndex = (index:number):number =>{
    return index;
  }

}
