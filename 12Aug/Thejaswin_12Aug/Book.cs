﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thejaswin_12Aug
{
    class Book
    {
        private int id;
        private string bookName;
        private double phno;

        public Book(int id,
                    string bookName,
                    double phno)
        {
            this.id = id;
            this.BookName = bookName;
            this.phno = phno;
        }

        public int Id { get => id; set => id = value; }
        public string BookName { get => bookName; set => bookName = value; }
        public double Phno { get => phno; set => phno = value; }
    }
}
