﻿using MediatR;
using Milestone.BusinessLayer.Services;
using Milestone.Commands;
using Milestone.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Milestone.Handlers
{
    public class AddProductHandler : IRequestHandler<AddProductCommand, IEnumerable<Product>>
    {
        IGroceries _data;

        public AddProductHandler(IGroceries data)
        {
            _data = data;
        }
        public async Task<IEnumerable<Product>> Handle(AddProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.AddProduct(request.Product));
        }
    }
}
