﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Thejaswin_MVCWebAPI_17Aug.Models;

namespace Thejaswin_MVCWebAPI_17Aug.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDateTime _dateTime;

        public HomeController(IDateTime dateTime)
        {
            _dateTime = dateTime;
        }

        public IActionResult Index()
        {
            var serverTime = _dateTime.Now;
            if (serverTime < 12) 
            {
                ViewData["Message"] = "Its Morning here - Good Morning!";
                    
            }
            return View();

        }

        //public IActionResult Index()
        //{
        //    return View();
        //}

        public IActionResult Details()
        {
            Student student = new Student()
            {
                studentId = 1,
                studentName = "Thejaswin",
                studentPhno = 9003554928
            };
            ViewData["Info"] = student;

            return View();
        }

        public IActionResult AutoDetails()
        {
            Automobile car = new Automobile()
            {
                cardID = 1,
                carname = "Benz",
                carProduction = 2022,
                carType = "Petrol"
            };
            ViewBag.carinfo = car;
            return View();
        }



    }
}
