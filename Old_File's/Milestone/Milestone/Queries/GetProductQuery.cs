﻿using MediatR;
using Milestone.Models;
using System.Collections.Generic;

namespace Milestone.Queries
{
    public class GetProductQuery : IRequest<IEnumerable<Product>>
    {
        /*
            No input 
        */
    }
}
