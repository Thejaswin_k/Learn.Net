﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Thejaswin_Employee_Crud.Models;

namespace Thejaswin_Employee_Crud.Controllers
{
    public class HomeController : Controller
    {
        public static List<Employee> employeeList = new List<Employee>();

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult AddEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(Employee input)
        {
            employeeList.Add(new Employee { employeeGuid = Guid.NewGuid(), employeeName = input.employeeName, employeeAddress = input.employeeAddress, employeeDepartment = input.employeeDepartment, employeePhno = input.employeePhno });
            return View();
        }

        public ViewResult DisplayEmployee()
        {
            ViewBag.employeeDetails = employeeList;
            return View();
        }
        [HttpPost]
        public IActionResult DeleteEmployee(Guid id)
        {
            employeeList.Remove(employeeList.SingleOrDefault(o => o.employeeGuid == id));
            return View();
        }

        public IActionResult EditEmployee()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
